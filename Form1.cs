﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ProgressBar;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TrackBar;

namespace ToDo
{
    public partial class Form1 : Form
    {
        CheckBox currentCheckBox;
        TimerWithCheckBox timerWithCheck1;
		public Form1()
        {
            InitializeComponent();
		}

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                CheckBox check = new CheckBox();
                check.Text = textBox1.Text;
                check.Parent = flowLayoutPanel1;
				check.CheckedChanged += checkBoxClicked;
				textBox1.Text = "";
                
            }
        }
        private void checkBoxClicked(object sender, EventArgs e)
        {
            TimerWithCheckBox timerWithCheck1 = new TimerWithCheckBox(sender as CheckBox, flowLayoutPanel1);
            timerWithCheck1.Start();
		}

    }
    public class TimerWithCheckBox : System.Windows.Forms.Timer
    {
        public CheckBox checkBox;
		public FlowLayoutPanel flowLayout;

		public TimerWithCheckBox(CheckBox checkBox, FlowLayoutPanel flowLayout) : base()
		{
            this.checkBox = checkBox;
			this.flowLayout = flowLayout;
			this.Interval = 750;
            this.Tick += tick;
		}
        public void tick(object sender, EventArgs e)
		{
			this.Stop();
			this.flowLayout.Controls.Remove(checkBox);
		}
	}
}
